@extends('admin.layout.layout')
@section('content')
<div class="card strpied-tabled-with-hover">
 <div class="card-header ">
  <h4 class="card-title">Sửa Quy Tắc</h4>
 </div>
 <div class="card-body">
  <form action="{{route('admin.updateAbout_us',$aboutus->id)}}" method="Post" enctype="multipart/form-data">
   @csrf
   <div class="form-group">
    <label for="">Tên</label>
    <input type="text" class="form-control" name="name" value="{{$aboutus->name}}">
   </div>
   <div class="form-group">
    <label for="">Email</label>
    <input type="text" class="form-control" name="email" value="{{$aboutus->email}}">
   </div>
   <div class="form-group">
    <label for="">SĐT</label>
    <input type="text" class="form-control" name="phone" value="{{$aboutus->phone}}">
   </div>
   <div class="form-group">
    <label for="">Địa Chỉ</label>
    <input type="text" class="form-control" name="address" value="{{$aboutus->address}}">
   </div>
   <div class="form-group">
    <label for="">Nội Dung</label>
    <input type="text" class="form-control" name="content" value="{{$aboutus->content}}">
   </div>
   <button type=" submit" class="btn btn-primary">Sửa</button>
 </div>
</div>
@endsection