@extends('admin.layout.layout')
@section('content')
<div class="card strpied-tabled-with-hover">
 <div class="card-header ">
  <h4 class="card-title">About_Us</h4>
 </div>
 <div class="card-body table-full-width table-responsive">
  <table class="table table-hover table-striped">
   <thead class="text-primary">
    <th class="text-center">ID</th>
    <th class="text-center">Tên</th>
    <th class="text-center">Email</th>
    <th class="text-center">SĐT</th>
    <th class="text-center">Địa Chỉ</th>
    <th class="text-center">Nội Dung </th>
    <!-- <th class="text-center">Tác Động</th> -->
   </thead>
   <tbody>
    @foreach($aboutus as $about_us)
    <tr>
     <td>{{$about_us->id}}</td>
     <td>{{$about_us->name}}</td>
     <td>{{$about_us->email}}</td>
     <td>{{$about_us->phone}}</td>
     <td>{{$about_us->address}}</td>
     <td>{{$about_us->content}}</td>
     <!-- <td class="">
      <a href="{{route('admin.editAbout_us', $about_us->id)}}" class="btn btn-success"><i class="fa fa-edit"></i></a>
      <a href="{{route('admin.deleteAbout_us', $about_us->id)}}" class="btn btn-danger"><i class="fa fa-trash"></i></a>
     </td> -->
    </tr>
    @endforeach
   </tbody>
  </table>
 </div>
</div>
@endsection