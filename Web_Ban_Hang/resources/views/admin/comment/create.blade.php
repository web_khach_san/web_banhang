<form action="{{ route('comment.postCreate') }}" method="post">
    @csrf
    <textarea name="content" style="width:100%"></textarea>
    <input type="submit" value="Bình luận">
</form>

<div class="container">

    @foreach ($comments as $comment)
    <div class="col-lg-12">
        <p>{{ $comment->content }}</p>
        <button id="reply{{ $comment->id }}">Trả lời</button>
        @foreach($comment->Rep_comment as $rep_comment)
            {{ $rep_comment->content }}
        @endforeach
        <form action="{{ route('repcomment.postCreate') }}" method="post" style="position:relative">
        @csrf
            <input type="text" style="width:100%;display:none" id="repcomment{{ $comment->id }}"
                value="{{ $comment->id }}" name="content" style="display:none">
            <input type="text" name="id_repComment" value="{{ $comment->id }}" hidden>
            <input type="submit" id="rep{{ $comment->id }}" value="rep" style="display:none">
            <a style="position:absolute; top:2px;right:10px;color:red;display:none" id="close{{ $comment->id }}">X</a>
        </form>
    </div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script>
        $(document).ready(function(){
            $("#reply{{ $comment->id }}").click(function(){
                $("#repcomment{{ $comment->id }}").show();
                $("#close{{ $comment->id }}").show();
                $("#rep{{ $comment->id }}").show();
            });
            $("#close{{ $comment->id }}").click(function(){
                $("#repcomment{{ $comment->id }}").hide();
                $("#close{{ $comment->id }}").hide();
                $("#rep{{ $comment->id }}").hide();
            });

        });
    </script>
    @endforeach

</div>