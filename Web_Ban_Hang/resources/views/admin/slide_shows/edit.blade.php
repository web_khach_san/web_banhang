@extends('admin.layout.layout')
@section('content')
<div class="card strpied-tabled-with-hover">
 <div class="card-header ">
  <h4 class="card-title">Sửa Slide_Shows</h4>
 </div>
 <div class="card-body">
  <form action="{{route('admin.updateSlide_show',$slides->id)}}" method="Post" enctype="multipart/form-data">
   @csrf
   <div class="form-group">
    <label for="">Tên</label>
    <input type="text" class="form-control" name="name" value="{{$slides->name}}">
   </div>
   <div class="form-group">
    <label>Ảnh</label>
    <img width="200px" src="{{asset('storage/'.$slides->imgSlide)}}">
    <input type="file" class="form-control" name="imgSlide" onclick="document.getElementById('selectedFile').click();">
   </div>
   <button type=" submit" class="btn btn-primary">Sửa</button>
 </div>
</div>
@endsection