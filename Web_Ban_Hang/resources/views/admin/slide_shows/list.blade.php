@extends('admin.layout.layout')
@section('content')
<div class="card strpied-tabled-with-hover">
 <div class="card-header ">
  <h4 class="card-title">Slide_Shows</h4>
  <a href="{{route('admin.createSlide_show')}}" class="btn btn-primary">Thêm</a>
 </div>
 <div class="card-body table-full-width table-responsive">
  <table class="table table-hover table-striped">
   <thead class="text-primary">
    <th class="text-center">ID</th>
    <th class="text-center">Tên </th>
    <th class="text-center">Ảnh</th>
    <th class="text-center">Tác Động</th>
   </thead>
   <tbody>
    @foreach($slides as $slide)
    <tr>
     <td>{{$slide->id}}</td>
     <td>{{$slide->name}}</td>
     <td><img src="{{asset('storage/'.$slide->image)}}" class="rounded-circle" style="width: 150px; height: 100px"></td>
     <td class="">
      <a href="{{route('admin.editSlide_show',$slide->id)}}" class="btn btn-success"><i class="fa fa-edit"></i></a>
      <a href="{{route('admin.deleteSlide_show',$slide->id)}}" class="btn btn-danger"><i class="fa fa-trash"></i></a>
     </td>
    </tr>
    @endforeach
   </tbody>
  </table>
 </div>
</div>
@endsection