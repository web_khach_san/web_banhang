@extends('admin.layout.layout')
@section('content')
<div class="card strpied-tabled-with-hover">
 <div class="card-header ">
  <h4 class="card-title">Thêm Ảnh</h4>
 </div>
 <div class="card-body">
  <form action="{{route('admin.storeSlide_show')}}" method="Post" enctype="multipart/form-data">
   @csrf
   <div class="form-group">
    <label for="">Tên</label>
    <input type="text" class="form-control" name="name" placeholder="Nhập tên...">
   </div>
   <div class="form-group">
    <label>Ảnh</label>
    <input type="file" class="form-control" name="imgSlide" onclick="document.getElementById('selectedFile').click();">
   </div>
   <button type="submit" class="btn btn-primary">Thêm</button>
 </div>
</div>
@endsection