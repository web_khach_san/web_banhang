@extends('admin.layout.layout')
@section('content')
<div class="card strpied-tabled-with-hover">
 <div class="card-header ">
  <h4 class="card-title">Danh Sách Tin Tức - Sự Kiện</h4>
  <a href="{{route('admin.createNew_Events')}}" class="btn btn-primary">Thêm</a>
 </div>
 <div class="card-body table-full-width table-responsive">
  <table class="table table-hover table-striped">
   <thead class="text-primary">
    <th class="text-center">ID</th>
    <th class="text-center">Ảnh</th>
    <th class="text-center">Chức vụ</th>
    <th class="text-center">Nội Dung</th>
    <th class="text-center">Tác Động</th>
   </thead>
   <tbody>
    @foreach($new_event as $new_events)
    <tr>
     <td>{{$new_events->id}}</td>
     <td><img src="{{asset('storage/'.$new_events->image)}}" class="rounded-circle" style="width: 150px; height: 100px">
     </td>
     <td>{{$new_events->title}}</td>
     <td>{{$new_events->content}}</td>
     <td class="">
      <a href="{{route('admin.editNew_Events',$new_events->id)}}" class="btn btn-success"><i class="fa fa-edit"></i></a>
      <a href="{{route('admin.deleteNew_Events',$new_events->id)}}" class="btn btn-danger"><i
        class="fa fa-trash"></i></a>
     </td>
    </tr>
    @endforeach
   </tbody>
  </table>
 </div>
</div>
@endsection