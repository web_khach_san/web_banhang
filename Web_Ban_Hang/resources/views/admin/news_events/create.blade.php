@extends('admin.layout.layout')
@section('content')
<div class="card strpied-tabled-with-hover">
 <div class="card-header ">
  <h4 class="card-title">Thêm Tin Tức-Sự Kiện</h4>
 </div>
 <div class="card-body">
  <form action="{{route('admin.storeNew_Events')}}" method="Post" enctype="multipart/form-data">
   @csrf
   <div class="form-group">
    <label>Ảnh</label>
    <input type="file" class="form-control" name="imgNewEvent"
     onclick="document.getElementById('selectedFile').click();">
   </div>
   <div class="form-group">
    <label for="">Chức Vụ</label>
    <input type="text" class="form-control" name="title" placeholder="Nhập chức vụ...">
   </div>
   <div class="form-group">
    <label for="">Nội Dung</label>
    <input type="text" class="form-control" name="content" placeholder="Nhập nội dung....">
   </div>
   <button type="submit" class="btn btn-primary">Thêm</button>
 </div>
</div>
@endsection