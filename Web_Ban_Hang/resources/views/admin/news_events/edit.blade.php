@extends('admin.layout.layout')
@section('content')
<div class="card strpied-tabled-with-hover">
 <div class="card-header ">
  <h4 class="card-title">Sửa Tin Tức-Sự Kiện</h4>
 </div>
 <div class="card-body">
  <form action="{{route('admin.updateNew_Events',$new_event->id)}}" method="Post" enctype="multipart/form-data">
   @csrf
   <div class="form-group">
    <label>Ảnh</label>
    <img width="200px" src="{{asset('storage/'.$new_event->imgNewEvent)}}">
    <input type="file" class="form-control" name="imgNewEvent"
     onclick="document.getElementById('selectedFile').click();">
   </div>
   <div class="form-group">
    <label for="">Chức Vụ</label>
    <input type="text" class="form-control" name="title" value="{{$new_event->title}}">
   </div>
   <div class=" form-group">
    <label for="">Nội Dung</label>
    <input type="text" class="form-control" name="content" value="{{$new_event->content}}">
   </div>
   <button type=" submit" class="btn btn-primary">Sửa</button>
 </div>
</div>
@endsection