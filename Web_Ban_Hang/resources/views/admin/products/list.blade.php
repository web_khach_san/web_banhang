@extends('admin.layout.layout')
@section('content')
<div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card strpied-tabled-with-hover">
                    <div class="card-header ">
                        <div class="row">
                            <div>
                                <h4 class="card-title">Danh sách sản phẩm</h4>
                            </div> &emsp;
                            <div>
                                <a class='btn btn-primary' href="{{route('themsanpham')}}">Thêm sản phẩm</a>
                            </div>
                           
                        </div>
                    </div>
                    <div class="card-body table-full-width table-responsive">
                        <table class="table table-hover table-striped">
                            <thead>
                                <th>Tên sản phẩm</th>
                                <th>Mô tả về sản phẩm</th>
                                <th>Loại sản phẩm</th>
                                <th>Ảnh sản phẩm</th>
                                <th>Giá </th>
                                <th colspan="2">Thao tác</th>
                            </thead>
                            <tbody>
                                @foreach ($products as $product)
                                <tr>
                                
                                        <td>{{$product->name}}</td>
                                        <td>{{$product->content}}</td>
                                        <td>{{$product->categories->name}}</td>
                                        <td>
                                            @if ($product->image)
                                            <img src="upload/imageProduct/{{$product->image}}" class="img-fluid" width="200px">
                                            @else
                                                {{"chưa có ảnh"}}
                                            @endif
                                       
                                        </td>    
                                        <td>{{$product->price}}</td>
                                        <td>
                                        <a href="{{route('suasanpham',$product->id)}}">Sửa</a>
                                        <a href="{{route('xoasanpham',$product->id)}}">Xóa</a>
                                        </td>
                                      
                                </tr>

                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="card strpied-tabled-with-hover">
                    <div class="card-header ">
                            <div class="row">
                                    <div>
                                        <h4 class="card-title">Danh sách loại sản phẩm</h4>
                                    </div> &emsp;
                                    <div>
                                    <a class='btn btn-primary' href="{{route('themloaisanpham')}}">Thêm loại sản phẩm</a>
                                    </div>
                                   
                                </div>
                    </div>
                    <div class="card-body table-full-width table-responsive">
                        <table class="table table-hover">
                            <thead>
                           
                                <th>Tên loại sản phẩm</th>
                                <th>Mô tả về loại sản phẩm</th>
                                <th>Ảnh về loại sản phẩm</th>
                                <th colspan="2">Thao tác</th>
                            </thead>
                            <tbody>
                                @foreach ($categories as $category)
                                    <tr>
                                        <td>{{$category->name}}</td>
                                        <td>{{$category->description}}</td>
                                        <td>
                                            @if ($category->image)
                                            <img src="upload/imageCategory/{{$category->image}}" class="img-fluid" width="200px">
                                            @else
                                               {{"chưa có ảnh"}} 
                                            @endif
                                            
                                        </td>
                                        <td>
                                            <a href="{{route('sualoaisanpham',$category->id)}}">Sửa</a>
                                            <a href="{{route('xoaloaisanpham',$category->id)}}">Xóa</a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
