@extends('admin.layout.layout')
@section('content')

<div class="container-fluid">

    <div class="col-lg-12">
        <h3>
            Thay đổi thông tin sản phẩm <strong></strong>
        </h3>
    </div>
    @if(session('message'))
        <div class="alert alert-success">
        <strong>{{session('message')}}</strong>
        </div>
    @endif
    <form action="{{route('suasanpham',$products->id)}}" method="Post" enctype="multipart/form-data">
                <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                    <div class="form-group">
                    <label>Tên sản phẩm</label>
                    <input type="text" class="form-control" name="name" value="{{$products->name}}">
                                @if($errors->has('name'))
                                    <div class="alert alert-danger">
                                        <strong>{{ $errors->first('name')}}</strong>
                                    </div>
                                @endif
                    </div>

                    <div class="form-group">
                        <label>Mô tả  về sản phẩm</label>
                            <textarea type="text" class="form-control" name="content" row="20">{{$products->content}}</textarea>
                                @if($errors->has('content'))
                                    <div class="alert alert-danger">
                                        <strong>{{ $errors->first('content')}}</strong>
                                    </div>
                                @endif
                    </div>
                    <div class="form-group">
                        <label>Ảnh sản phẩm</label><br/>
                        <input type="file" name="imageProduct">
                        <img src="upload/imageProduct/{{$products->image}}" alt="">
                    </div>

                    <div class="form-group">
                            <label>Giá sản phẩm</label>
                                <input type="text" class="form-control" name="price"  value="{{$products->price}}">
                                    @if($errors->has('price'))
                                        <div class="alert alert-danger">
                                            <strong>{{ $errors->first('price')}}</strong>
                                        </div>
                                    @endif
                    </div>
                    <div class="form-group">
                            <label>Loại sản phẩm</label>
                                <select class="form-control" name="styleCategory">
                                        @foreach($categories as $category)
                                            <option 
                                                @if ($products->id_product == $category->id)
                                                {{ 'selected' }}
                                                @endif
                                                value={{$category->id}}   
                                            >
                                                {{$category->name}}
                                            </option>
                                        @endforeach
                                </select>
                                @if($errors->has('styleCategory'))
                                        <div class="alert alert-danger">
                                            <strong>{{ $errors->first('styleCategory')}}</strong>
                                        </div>
                                @endif
                    </div>
                    <button type="submit" class="btn btn-primary">Sửa sản phẩm</button>
            </form>
</div>
@endsection
