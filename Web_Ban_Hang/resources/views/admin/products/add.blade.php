@extends('admin.layout.layout')
@section('content')

<div class="container-fluid">

    <div class="col-lg-12">
        <h3>
            Thêm sản phẩm <strong></strong>
        </h3>
    </div>
    @if(session('message'))
        <div class="alert alert-success">
        <strong>{{session('message')}}</strong>
        </div>
    @endif
            <form action="{{route('themsanpham')}}" method="Post" enctype="multipart/form-data">
                <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                    <div class="form-group">
                    <label>Tên sản phẩm</label>
                    <input type="text" class="form-control" name="name" placeholder="Nhập tên sản phẩm">
                                @if($errors->has('name'))
                                    <div class="alert alert-danger">
                                        <strong>{{ $errors->first('name')}}</strong>
                                    </div>
                                @endif
                    </div>

                    <div class="form-group">
                        <label>Mô tả  về sản phẩm</label>
                            <textarea type="text" class="form-control ckeditor" name="content" row="20" placeholder="Mô tả về sản phẩm"></textarea>
                                @if($errors->has('content'))
                                    <div class="alert alert-danger">
                                        <strong>{{ $errors->first('content')}}</strong>
                                    </div>
                                @endif
                    </div>
                    <div class="form-group">
                        <label>Ảnh sản phẩm</label><br/>
                            <input type="file"  name="imageProduct">
                    </div>

                    <div class="form-group">
                            <label>Giá sản phẩm</label>
                                <input type="text" class="form-control" name="price"  placeholder="Giá của sản phẩm">
                                    @if($errors->has('price'))
                                        <div class="alert alert-danger">
                                            <strong>{{ $errors->first('price')}}</strong>
                                        </div>
                                    @endif
                    </div>
                    <div class="form-group">
                            <label>Loại sản phẩm</label>
                                <select class="form-control" name="styleCategory">
                                        @foreach($categories as $category)
                                        <option value="{{ $category->id }}">{{ $category->name }}</option>
                                        @endforeach
                                        </option>
                                </select>
                                @if($errors->has('styleCategory'))
                                        <div class="alert alert-danger">
                                            <strong>{{ $errors->first('styleCategory')}}</strong>
                                        </div>
                                @endif
                    </div>
                    <button type="submit" class="btn btn-primary">Thêm sản phẩm</button>
            </form>
</div>
@endsection
