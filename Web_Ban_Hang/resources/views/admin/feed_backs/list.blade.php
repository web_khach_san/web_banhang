@extends('admin.layout.layout')
@section('content')
<div class="card strpied-tabled-with-hover">
 <div class="card-header ">
  <h4 class="card-title">Danh Sách Phản Hồi</h4>
 </div>
 <div class="card-body table-full-width table-responsive">
  <table class="table table-hover table-striped">
   <thead class="text-primary">
    <th class="text-center">ID</th>
    <th class="text-center">Tên</th>
    <th class="text-center">Email</th>
    <th class="text-center">SĐT</th>
    <th class="text-center">Nội Dung </th>
    <th class="text-center">Tác Động</th>
   </thead>
   <tbody>
    @foreach($feed_back as $fb)
    <tr>
     <td>{{$fb->id}}</td>
     <td>{{$fb->name}}</td>
     <td>{{$fb->email}}</td>
     <td>{{$fb->phone}}</td>
     <td>{{$fb->content}}</td>
     <td class="">
      <a href="{{route('admin.deleteFeedBack', $fb->id)}}" class="btn btn-danger"><i class="fa fa-trash"></i></a>
     </td>
    </tr>
    @endforeach
   </tbody>
  </table>
 </div>
</div>
@endsection