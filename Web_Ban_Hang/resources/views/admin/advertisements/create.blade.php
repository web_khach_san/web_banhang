@extends('admin.layout.layout')
@section('content')
<div class="card strpied-tabled-with-hover">
 <div class="card-header ">
  <h4 class="card-title">Thêm Quảng Cáo</h4>
 </div>
 <div class="card-body">
  <form action="{{route('admin.storeAds')}}" method="Post" enctype="multipart/form-data">
   @csrf
   <div class="form-group">
    <label for="">Tên Quảng Cáo</label>
    <input type="text" class="form-control" name="name" placeholder="Nhập tên quảng cáo...">
   </div>
   <div class="form-group">
    <label>Ảnh</label>
    <input type="file" class="form-control" name="imageAds" onclick="document.getElementById('selectedFile').click();">
   </div>
   <div class="form-group">
    <label for="">Địa Chỉ</label>
    <input type="text" class="form-control" name="address" placeholder="Nhập địa chỉ...">
   </div>
   <div class="form-group">
    <label for="">SĐT</label>
    <input type="number" class="form-control" name="phone" placeholder="Nhập số điện thoại ....">
   </div>
   <div class="form-group">
    <label for="">Website</label>
    <input type="text" class="form-control" name="web" placeholder="Nhập website...">
   </div>
   <button type="submit" class="btn btn-primary">Thêm</button>
 </div>
</div>
@endsection