@extends('admin.layout.layout')
@section('content')
<div class="card strpied-tabled-with-hover">
 <div class="card-header ">
  <h4 class="card-title">Danh Sách Quảng Cáo</h4>
  <a href="{{route('admin.createAds')}}" class="btn btn-primary">Thêm</a>
 </div>
 <div class="card-body table-full-width table-responsive">
  <table class="table table-hover table-striped">
   <thead class="text-primary">
    <th class="text-center">ID</th>
    <th class="text-center">Tên Quảng Cáo</th>
    <th class="text-center">Ảnh</th>
    <th class="text-center">Địa Chỉ</th>
    <th class="text-center">SĐT</th>
    <th class="text-center">Website</th>
    <th class="text-center">Tác Động</th>
   </thead>
   <tbody>
    @foreach($ads as $ad)
    <tr>
     <td>{{$ad->id}}</td>
     <td>{{$ad->name}}</td>
     <td><img src="{{asset('storage/'.$ad->image)}}" class="rounded-circle" style="width: 150px; height: 100px"></td>
     <td>{{$ad->address}}</td>
     <td>{{$ad->phone}}</td>
     <td>{{$ad->web}}</td>
     <td class="">
      <a href="{{route('admin.editAds',$ad->id)}}" class="btn btn-success"><i class="fa fa-edit"></i></a>
      <a href="{{route('admin.deleteAds',$ad->id)}}" class="btn btn-danger"><i class="fa fa-trash"></i></a>
     </td>
    </tr>
    @endforeach
   </tbody>
  </table>
 </div>
</div>
@endsection