@extends('admin.layout.layout')
@section('content')
<div class="card strpied-tabled-with-hover">
 <div class="card-header ">
  <h4 class="card-title">Sửa Quảng Cáo</h4>
 </div>
 <div class="card-body">
  <form action="{{route('admin.updateAds',$ads->id)}}" method="Post" enctype="multipart/form-data">
   @csrf
   <div class="form-group">
    <label for="">Tên Quảng Cáo</label>
    <input type="text" class="form-control" name="name" value="{{$ads->name}}">
   </div>
   <div class="form-group">
    <label>Ảnh</label>
    <img width="200px" src="{{asset('storage/'.$ads->imageAds)}}">
    <input type="file" class="form-control" name="imageAds" onclick="document.getElementById('selectedFile').click();">
   </div>
   <div class="form-group">
    <label for="">Địa Chỉ</label>
    <input type="text" class="form-control" name="address" value="{{$ads->address}}">
   </div>
   <div class=" form-group">
    <label for="">SĐT</label>
    <input type="number" class="form-control" name="phone" value="{{$ads->phone}}">
   </div>
   <div class=" form-group">
    <label for="">Website</label>
    <input type="text" class="form-control" name="web" value="{{$ads->web}}">
   </div>
   <button type=" submit" class="btn btn-primary">Sửa</button>
 </div>
</div>
@endsection