<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <base href="{{ asset('') }}">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>ĐĂNG NHẬP</title>
    <link rel="stylesheet" href="Login/style.css">
</head>

<body>
    <div class="box">
        <h2>ĐĂNG NHẬP</h2>
        <form action="{{ route('user.postLogin') }}" method="POST" role="form">
            @csrf
            <div class="inputBox">
                                
                <input type="email" name="email"  placeholder="Email" required="">

                @if($errors->has('email'))
                    <div class="alert alert-danger">
                        <strong style="color:red">{{ $errors->first('email')}}</strong>
                    </div>
                    @endif
            </div>
            <div class="inputBox">
                <input type="password" placeholder="Mật khẩu" name="password" required="">
           
                @if($errors->has('password'))
                    <div class="alert alert-danger">
                        <strong style="color:red">{{ $errors->first('password')}}</strong>
                    </div>
                    @endif
            </div>
            
            @if(Session::has('thatbai'))
                    <div class="alert alert-danger">
                        <strong style="color:red">{{Session::get('thatbai')}}</strong>
                    </div>
                    @endif
                <p>    <input type="checkbox" name="remember"><span style="color:#ffffff">Ghi nhớ tài khoản</span></p>
            <input type="submit" name="" value="Đăng Nhập">
        </form>
    </div>
</body>

</html>