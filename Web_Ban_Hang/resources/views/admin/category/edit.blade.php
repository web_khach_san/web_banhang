@extends('admin.layout.layout')
@section('content')

<div class="container-fluid">

    <div class="col-lg-12">
        <h3>
            Sửa loại sản phẩm <strong></strong>
        </h3>
    </div>
    @if(session('message'))
        <div class="alert alert-success">
        <strong>{{session('message')}}</strong>
        </div>
    @endif
            <form action="{{route('sualoaisanpham',$categories->id)}}" method="Post" enctype="multipart/form-data">
                <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                    <div class="form-group">
                    <label>Tên loại sản phẩm</label>
                    <input type="text" class="form-control" name="name" value="{{$categories->name}}">
                                @if($errors->has('name'))
                                    <div class="alert alert-danger">
                                        <strong>{{ $errors->first('name')}}</strong>
                                    </div>
                                @endif
                    </div>

                    <div class="form-group">
                        <label>Mô tả  về loại sản phẩm</label>
                            <textarea type="text" class="form-control" name="description" row="20">{{$categories->name}}</textarea>
                                @if($errors->has('description'))
                                    <div class="alert alert-danger">
                                        <strong>{{ $errors->first('description')}}</strong>
                                    </div>
                                @endif
                    </div>
                    <div class="form-group">
                        <label>Ảnh loại sản phẩm</label><br/>
                            <input type="file"  name="imageCategory"><br/>
                            <img src="upload/imageCategory/{{$categories->image}}" alt="">
                    </div>
                    <button type="submit" class="btn btn-primary">Thêm loại sản phẩm</button>
            </form>
</div>
@endsection
