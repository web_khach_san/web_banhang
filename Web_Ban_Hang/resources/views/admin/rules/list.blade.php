@extends('admin.layout.layout')
@section('content')
<div class="card strpied-tabled-with-hover">
 <div class="card-header ">
  <h4 class="card-title">Quy Tắc</h4>
  <a href="{{route('admin.createRules')}}" class="btn btn-primary">Thêm</a>
 </div>
 <div class="card-body table-full-width table-responsive">
  <table class="table table-hover table-striped">
   <thead class="text-primary">
    <th class="text-center">ID</th>
    <th class="text-center">Nội Dung </th>
    <th class="text-center">Tác Động</th>
   </thead>
   <tbody>
    @foreach($rules as $rule)
    <tr>
     <td>{{$rule->id}}</td>
     <td>{{$rule->content}}</td>
     <td class="">
      <a href="{{route('admin.editRules', $rule->id)}}" class="btn btn-success"><i class="fa fa-edit"></i></a>
      <a href="{{route('admin.deleteRules', $rule->id)}}" class="btn btn-danger"><i class="fa fa-trash"></i></a>
     </td>
    </tr>
    @endforeach
   </tbody>
  </table>
 </div>
</div>
@endsection