@extends('admin.layout.layout')
@section('content')
<div class="card strpied-tabled-with-hover">
 <div class="card-header ">
  <h4 class="card-title">Sửa Quy Tắc</h4>
 </div>
 <div class="card-body">
  <form action="{{route('admin.updateRules',$rules->id)}}" method="Post" enctype="multipart/form-data">
   @csrf
   <div class="form-group">
    <label for="">Nội Dung</label>
    <input type="text" class="form-control" name="content" value="{{$rules->content}}">
   </div>
   <button type=" submit" class="btn btn-primary">Sửa</button>
 </div>
</div>
@endsection