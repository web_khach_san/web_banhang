@extends('admin.layout.layout')
@section('content')
<div class="card strpied-tabled-with-hover">
 <div class="card-header ">
  <h4 class="card-title">Thêm Quy Tắc</h4>
 </div>
 <div class="card-body">
  <form action="{{route('admin.storeRules')}}" method="Post" enctype="multipart/form-data">
   @csrf
   <div class="form-group">
    <label for="">Nội Dung</label>
    <input type="text" class="form-control" name="content" placeholder="Nhập nội dung...">
   </div>
   <button type="submit" class="btn btn-primary">Thêm</button>
 </div>
</div>
@endsection