<div class="sidebar-wrapper">
 <div class="logo">
  <a href="http://www.creative-tim.com" class="simple-text">
   Creative Tim
  </a>
 </div>
 <ul class="nav">
  <li class="nav-item active">
   <a class="nav-link" href="{{route('admin.About_us')}}">
    <i class="nc-icon nc-chart-pie-35"></i>
    <p>About_us</p>
   </a>
  </li>
  <li>
   <a class="nav-link" href="./user.html">
    <i class="nc-icon nc-circle-09"></i>
    <p>User Profile</p>
   </a>
  </li>
  <li>
   <a class="nav-link" href="{{route('admin.DSquangcao')}}">
    <i class="nc-icon nc-notes"></i>
    <p>Quản Lý Quảng Cáo</p>
   </a>
  </li>
  <li>
   <a class="nav-link" href="{{route('admin.DsNew_Events')}}">
    <i class="nc-icon nc-notes"></i>
    <p>Tin Tức-Sự Kiện</p>
   </a>
  </li>
  <li>
   <a class="nav-link" href="{{route('admin.DsSlide_show')}}">
    <i class="nc-icon nc-notes"></i>
    <p>Slide_Shows</p>
   </a>
  </li>
  <li>
   <a class="nav-link" href="{{route('admin.DsQuytac')}}">
    <i class="nc-icon nc-paper-2"></i>
    <p>Danh Sách Quy Tắc</p>
   </a>
  </li>
  <li>
   <a href="{{route('admin.DsFeedBack')}}" class="nav-link">
    <i class="nc-icon nc-paper-2"></i>
    <p>Danh Sách Phản Hồi</p>
   </a>
  </li>
  <li>
   <a class="nav-link" href="./icons.html">
    <i class="nc-icon nc-atom"></i>
    <p>Icons</p>
   </a>
  </li>
  <li>
   <a class="nav-link" href="./maps.html">
    <i class="nc-icon nc-pin-3"></i>
    <p>Maps</p>
   </a>
  </li>
  <li>
   <a class="nav-link" href="./notifications.html">
    <i class="nc-icon nc-bell-55"></i>
    <p>Notifications</p>
   </a>
  </li>
  <li class="nav-item active active-pro">
   <a class="nav-link active" href="upgrade.html">
    <i class="nc-icon nc-alien-33"></i>
    <p>Upgrade to PRO</p>
   </a>
  </li>
 </ul>
</div>