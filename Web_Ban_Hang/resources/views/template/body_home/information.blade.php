<div class="col-12 col-lg-6">
    <div class="row">
        <!-- Single Post -->
        @foreach ($productType as $product)
        <div class="col-12">
            <div class="single-post wow fadeInUp" data-wow-delay=".2s">
                <!-- Post Thumb -->
                <div class="post-thumb">
                    <img src="upload/imageProduct/{{$product->image}}" width="100%" height="100%" alt="">
                </div>
                <!-- Post Content -->
               
            </div>
        </div>
        @endforeach
       

    </div>
</div>
<div class="col-12 col-sm-8 col-md-6 col-lg-6">
    <div class="blog-sidebar mt-5 mt-lg-0">
        <!-- Single Widget Area -->
        <div class="single-widget-area about-me-widget text-center">
            <div class="widget-title">
                <h6>About Me</h6>
            </div>
            <div class="about-me-widget-thumb">
                <img src="plugin/img/about-img/1.jpg" alt="">
            </div>
            <h4 class="font-shadow-into-light">Shopia Bernard</h4>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do
                eiusmod tempor incididunt</p>
        </div>
    </div>
   
</div>