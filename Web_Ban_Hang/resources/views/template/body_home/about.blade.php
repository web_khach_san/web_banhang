<section class="categories_area clearfix" id="about">
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-6 col-lg-4">
                <div class="single_catagory wow fadeInUp" data-wow-delay=".3s">
                    <img src="plugin/img/catagory-img/1.jpg" alt="">
                    <div class="catagory-title">
                        <a href="#">
                            <h5>Giới thiệu</h5>
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-6 col-lg-4">
                <div class="single_catagory wow fadeInUp" data-wow-delay=".6s">
                    <img src="plugin/img/catagory-img/2.jpg" alt="">
                    <div class="catagory-title">
                        <a href="#">
                            <h5>Lãnh đạo</h5>
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-6 col-lg-4">
                <div class="single_catagory wow fadeInUp" data-wow-delay=".9s">
                    <img src="plugin/img/catagory-img/3.jpg" alt="">
                    <div class="catagory-title">
                        <a href="#">
                            <h5>Lịch sử phát triển</h5>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>