<!DOCTYPE html>
<html lang="en">

<head>
 <meta charset="UTF-8">
 <meta name="description" content="">
 <meta http-equiv="X-UA-Compatible" content="IE=edge">
 <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
 <title>Web bán hàng</title>
 @include('template.link.link')
</head>

<body>
        <div id="preloader">
        <div class="yummy-load"></div>
 </div>
        @include('template.header.header_top')
        @include('template.header.header_bottom')
        
        @yield('banner')
        @yield('content')
        @include('template.body_home.about')
        
        @include('template.footer.footer')
        @include('template.link.script')
</body>
