<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comments', function (Blueprint $table) {
            $table->increments('id');
            $table->text('content');
            $table->string('image',191)->nullable();
            $table->timestamps();
        });
        Schema::table('comments', function (Blueprint $table) {
            $table->unsignedInteger('id_user')->after('id')->nullable()->onDelete('cascade'); 
            $table->foreign('id_user')->references('id')->on('users');
        });
        Schema::table('rep_comments', function (Blueprint $table) {
            $table->unsignedInteger('id_repComment')->after('id')->nullable()->onDelete('cascade'); 
            $table->foreign('id_repComment')->references('id')->on('comments');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('comments');
    }
}
