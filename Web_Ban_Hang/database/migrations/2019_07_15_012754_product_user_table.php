<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ProductUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_user', function (Blueprint $table) {
            $table->increments('id');
        });
        Schema::table('product_user', function (Blueprint $table) {
            $table->unsignedInteger('id_user')->after('id')->onDelete('cascade'); 
            $table->foreign('id_user')->references('id')->on('users');
        });
        Schema::table('product_user', function (Blueprint $table) {
            $table->unsignedInteger('id_product')->after('id')->onDelete('cascade'); 
            $table->foreign('id_product')->references('id')->on('products');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_user');
    }
}
