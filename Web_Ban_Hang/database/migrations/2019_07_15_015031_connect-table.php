<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ConnectTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table("products", function (Blueprint $table) { 
            $table->foreign('id_categories')->references('id')->on('categories')->onDelete('cascade');   
        });
        // Schema::table("products", function (Blueprint $table) { 
        //     $table->foreign('id_order')->references('id')->on('orders')->onDelete('cascade');   
        // });
        Schema::table("images", function (Blueprint $table) { 
            $table->foreign('id_product')->references('id')->on('products')->onDelete('cascade');   
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
