<?php

use Illuminate\Database\Seeder;

class News_EventsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('news_events')->insert([
            'image' => "http://www.nuibavi.com/nuibavi-images/news/img1/chieu-ba-vi-dep.jpg",
            'title' => "CEO",
            'content' => "Sky_Blue",
        ]);
    }
}