<?php

use Illuminate\Database\Seeder;

class Slide_ShowsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('slide_shows')->insert([
            'name' => "Ảnh 1",
            'image' => "http://nguyentrieu.net/upload/images/2017/_Thang_09/img_album1.png",
        ]);
    }
}