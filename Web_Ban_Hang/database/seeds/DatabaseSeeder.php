<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(ProductTableSeeder::class);
        $this->call(AdvertisementsTableSeeder::class);
        $this->call(Slide_ShowsTableSeeder::class);
        $this->call(About_UsTableSeeder::class);
        $this->call(Feed_BackTableSeeder::class);
        $this->call(News_EventsTableSeeder::class);
        $this->call(RulesTableSeeder::class);
    }
}