<?php

use Illuminate\Database\Seeder;

class AdvertisementsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('advertisements')->insert([
            'name' => "Mệ Bụi",
            'image' => "../plugin/img/mebui/1.jpg",
            'address' => "Kiệt 27 Hoàng Quốc Việt, Huế",
            'phone' => "0932.522882",
            'web' => "mebui.com",
        ]);
    }
}