<?php

use Illuminate\Database\Seeder;

class About_UsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('about_us')->insert([
            'name' => "Bánh Nguyễn Triều",
            'email' => "nguyentrieu2019@gmail.com",
            'phone' => "0914152702",
            'address' => "26 Nguyễn Đức Tịnh",
            'content' => "Mỗi chiếc bánh là nơi đúc kết tâm tình, là tấm lòng tri ân, là câu chuyện kể về một loại cây trái đặc thù, là sản vật nổi tiếng của vùng đất Cố đô: sen hồ Tịnh, thanh trà Thủy Biều, quýt Hương Cần, hay quả vả riêng có ở xứ Huế v.v. Đó không chỉ là món ăn mà còn là văn hóa, là truyền thống, là di sản ẩm thực cung đình Huế được những người trẻ tuổi hôm nay kế thừa và tiếp bước cha ông. ",
        ]);
    }
}