<?php

Route::get('', 'HomeController@trangchu')->name('trangchu');

Route::get('add-to-cart/{id}', 'HomeController@getAddToCart')->name('addCart');

Route::group(['prefix' => 'users'], function () {
    Route::get('create', 'UserController@Create')->name('user.create');
    Route::post('create', 'UserController@postCreate')->name('user.postCreate');
    Route::get('login', 'UserController@login')->name('user.login');
    Route::post('login', 'UserController@postLogin')->name('user.postLogin');
    Route::get('logout', 'UserController@logoutUser')->name('user.logout');
});

Route::group(['prefix' => 'comment'], function () {
    Route::get('create', 'CommentController@createComment')->name('comment.create');
    Route::post('create', 'CommentController@postComment')->name('comment.postCreate');
    // Route::get('edit/{id}','CommentController@editComment')->name('comment.edit');
    // Route::post('edit/{id}','CommentController@updateComment')->name('comment.update');
    // Route::get('delete/{id}','CommentController@deleteComment')->name('comment.delete');
});

Route::group(['prefix' => 'repcomment'], function () {
    Route::get('create', 'RepCommentsController@createRepComment')->name('repcomment.create');
    Route::post('create', 'RepCommentsController@postRepComment')->name('repcomment.postCreate');
    // Route::get('edit/{id}','RepCommentsController@editComment')->name('repcomment.edit');
    // Route::post('edit/{id}','RepCommentsController@updateComment')->name('repcomment.update');
    // Route::get('delete/{id}','RepCommentsController@deleteComment')->name('repcomment.delete');
});

Route::group(['prefix' => 'admin'], function () {

    Route::group(['prefix' => 'san-pham'], function () {

Route::group(['prefix' => 'admin'], function () {
    Route::get('', 'HomeController@admin')->name('admin');

    Route::group(['prefix' => 'ads'], function () {
        Route::get('DSquangcao', 'AdvertisementController@DSquangcao')->name('admin.DSquangcao');
        Route::get('addAds', 'AdvertisementController@createAds')->name('admin.createAds');
        Route::post('storeAds', 'AdvertisementController@storeAds')->name('admin.storeAds');
        Route::get('deleteAds/{id}', 'AdvertisementController@deleteAds')->name('admin.deleteAds');
        Route::get('editAds/{id}', 'AdvertisementController@editAds')->name('admin.editAds');
        Route::post('editAds/{id}', 'AdvertisementController@updateAds')->name('admin.updateAds');
    });

    Route::group(['prefix' => 'slide_show'], function () {
        Route::get('DsSlide_show', 'Slide_ShowController@DsSlide_show')->name('admin.DsSlide_show');
        Route::get('createSlide_show', 'Slide_ShowController@createSlide_show')->name('admin.createSlide_show');
        Route::post('storeSlide_show', 'Slide_ShowController@storeSlide_show')->name('admin.storeSlide_show');
        Route::get('deleteSlide_show/{id}', 'Slide_ShowController@deleteSlide_show')->name('admin.deleteSlide_show');
        Route::get('editSlide_show/{id}', 'Slide_ShowController@editSlide_show')->name('admin.editSlide_show');
        Route::post('editSlide_show/{id}', 'Slide_ShowController@updateSlide_show')->name('admin.updateSlide_show');
    });

    Route::group(['prefix' => 'rules'], function () {
        Route::get('DsQuytac', 'RulesController@DsQuytac')->name('admin.DsQuytac');
        Route::get('createRules', 'RulesController@createRules')->name('admin.createRules');
        Route::post('storeRules', 'RulesController@storeRules')->name('admin.storeRules');
        Route::get('deleteRules/{id}', 'RulesController@deleteRules')->name('admin.deleteRules');
        Route::get('editRules/{id}', 'RulesController@editRules')->name('admin.editRules');
        Route::post('editRules/{id}', 'RulesController@updateRules')->name('admin.updateRules');
    });

    Route::group(['prefix' => 'about_us'], function () {
        Route::get('About_us', 'AboutUsController@About_us')->name('admin.About_us');
    });
    Route::group(['prefix' => 'feed_back'], function () {
        Route::get('DsFeedBack', 'FeedBackController@DsFeedBack')->name('admin.DsFeedBack');
        Route::get('deleteFeedBack/{id}', 'FeedBackController@deleteFeedBack')->name('admin.deleteFeedBack');
    });

    Route::group(['prefix' => 'news_events'], function () {
        Route::get('DsNew_Events', 'NewsEventsController@DsNew_Events')->name('admin.DsNew_Events');
        Route::get('createNew_Events', 'NewsEventsController@createNew_Events')->name('admin.createNew_Events');
        Route::post('storeNew_Events', 'NewsEventsController@storeNew_Events')->name('admin.storeNew_Events');
        Route::get('deleteNew_Events/{id}', 'NewsEventsController@deleteNew_Events')->name('admin.deleteNew_Events');
        Route::get('editNew_Events/{id}', 'NewsEventsController@editNew_Events')->name('admin.editNew_Events');
        Route::post('editNew_Events/{id}', 'NewsEventsController@updateNew_Events')->name('admin.updateNew_Events');
    });
});
