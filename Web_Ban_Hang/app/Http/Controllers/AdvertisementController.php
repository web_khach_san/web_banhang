<?php

namespace App\Http\Controllers;

use App\Advertisement;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class AdvertisementController extends Controller
{
    public function DSquangcao()
    {
        $ads = Advertisement::all();
        return view('admin.advertisements.list', compact('ads'));
    }

    public function createAds()
    {
        return view('admin.advertisements.create');
    }

    public function storeAds(Request $request)
    {
        $ads = new Advertisement();

        $ads->name = $request->name;
        if ($request->hasFile('imageAds')) {
            $image = $request->file('imageAds');
            $path = $image->store('adsImage', 'public');
            $ads->image = $path;
        }
        $ads->address = $request->address;
        $ads->phone = $request->phone;
        $ads->web = $request->web;
        $ads->save();
        return redirect('admin/ads/DSquangcao')->with('message', 'Thêm thành công!');
    }


    public function deleteAds($id)
    {

        $ads = Advertisement::find($id);
        $image = $ads->image;
        if ($image) {
            Storage::delete('/public/' . $image);
        }
        $ads->delete();
        return redirect()->back()->with('message', 'Xóa thành công!');
    }

    public function editAds($id)
    {
        $ads = Advertisement::findOrFail($id);
        return view('admin.advertisements.edit', compact('ads'));
    }

    public function updateAds(Request $request, $id)
    {
        $ads = Advertisement::findOrFail($id);

        $ads->name = $request->name;
        if ($request->hasFile('imageAds')) {
            $image = $request->file('imageAds');
            $path = $image->store('adsImage', 'public');
            $ads->image = $path;
        }
        $ads->address = $request->address;
        $ads->phone = $request->phone;
        $ads->web = $request->web;

        $ads->save();

        return redirect('admin/ads/DSquangcao')->with('message', 'Sửa thành công!');
    }
}