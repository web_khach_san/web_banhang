<?php

namespace App\Http\Controllers;

use App\RepComments;
use Illuminate\Http\Request;

class RepCommentsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function createRepComment()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function postRepComment(Request $request)
    {
        $repcomment = new RepComments;
        $repcomment->content = $request->content;
        $repcomment->id_repComment = $request->id_repComment;
        $repcomment->save();
        
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\RepComments  $repComments
     * @return \Illuminate\Http\Response
     */
    public function show(RepComments $repComments)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\RepComments  $repComments
     * @return \Illuminate\Http\Response
     */
    public function edit(RepComments $repComments)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\RepComments  $repComments
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, RepComments $repComments)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\RepComments  $repComments
     * @return \Illuminate\Http\Response
     */
    public function destroy(RepComments $repComments)
    {
        //
    }
}
