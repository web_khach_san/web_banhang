<?php

namespace App\Http\Controllers;

use App\Product;
use App\Category;
use Illuminate\Http\Request;
use File;

class ProductController extends Controller
{
  
    public function danhSachSanPham()
    {
       $products = Product::all();
       $categories = Category::all();
       return view('admin.products.list',compact('products','categories'));
    }
    public function themSanPham(){
        $categories = Category::all();
        return view('admin.products.add',compact('categories'));
    }
    public function XuLyThemSanPham(Request $request)
    {
        $this->validate($request,
            [
                'name' => 'required',
                'content' => 'required|min:10',
                'price' => 'required',
            ],
            [
                'name.required' => 'Bạn chưa nhập tên sản phẩm!',
                'content.required' => 'Bạn chưa nhập mô tả về sản phẩm!',
                'content.min' => 'Tên quá ngắn, yêu cầu tối thiểu gồm 10 ký tự trở lên!',
                'price.required' => 'Bạn chưa nhập giá sản phẩm!',
            ]);
        $products = new Product();

        $products->name = $request->name;
        $products->content = $request->content;
        if($request->hasFile('imageProduct')) 
    	{
    		$img_file = $request->file('imageProduct'); 
    		$img_file_name = $img_file->getClientOriginalName();
    		$img_file->move('upload/imageProduct',$img_file_name); 
    		$products->image = $img_file_name;
    	}
    	else
    		$products->image = ''; 
        $products->price = $request->price;
        $products->id_categories = $request->styleCategory;
    
        
        $products->save();

        return redirect('admin/san-pham/danh-sach-san-pham')->with('message', 'Thêm thành công!');
    }
    public function suaSanPham($id)
    {
        $products = Product::find($id);
        $categories = Category::all();
        return view('admin.products.edit',compact('products','categories'));
    }

    public function xuLySuaSanPham(Request $request, $id)
    {
        $this->validate($request,
            [
                'name' => 'required',
                'content' => 'required|min:10',
                'price' => 'required',
            ],
            [
                'name.required' => 'Bạn chưa nhập tên sản phẩm!',
                'content.required' => 'Bạn chưa nhập mô tả về sản phẩm!',
                'content.min' => 'Tên quá ngắn, yêu cầu tối thiểu gồm 10 ký tự trở lên!',
                'price.required' => 'Bạn chưa nhập giá sản phẩm!',
            ]);
        $products =  Product::find($id);

        $products->name = $request->name;
        $products->content = $request->content;
        if($request->hasFile('imageProduct')) 
    	{
    		$img_file = $request->file('imageProduct'); 
            $img_file_name = $img_file->getClientOriginalName();
            if(File::exists('upload/imageProduct/'.$products->image)){
                unlink('upload/imageProduct/'.$products->image);
                $img_file->move('upload/imageProduct',$img_file_name); 
                $products->image = $img_file_name;
            } else{
                $img_file->move('upload/imageProduct',$img_file_name); 
    		    $products->image = $img_file_name;
            }
        }
        $products->price = $request->price;
        $products->id_categories = $request->styleCategory;
        $products->save();
        return redirect('admin/san-pham/danh-sach-san-pham')->with('message', 'Cập nhập thành công!');
    }

    public function xoaSanPham($id)
    {
        $products =  Product::find($id);
        if(File::exists('upload/imageProduct/'.$products->image)){
            unlink('upload/imageProduct/'.$products->image);
        }
        $products->delete();
        return redirect('admin/san-pham/danh-sach-san-pham')->with('message', 'Xóa thành công!');
    }
}
   
