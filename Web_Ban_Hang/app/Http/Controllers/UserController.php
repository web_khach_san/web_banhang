<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    public function Create()
    {
        return view('admin.users.create');
    }

    public function postCreate(Request $request)
    {
        $this->validate($request,
      [
        'name'=>'required',
        'address' =>'required' ,
        'email' =>'required|unique:users',
        'password' =>'required|min:6' ,
  
       
      ],
      [
        'name.required' => 'Bạn chưa nhập tên!',
        'password.required' => 'Bạn chưa nhập mật khẩu!',
        'password.min' => 'Mật khẩu phải có ít nhất 6 kí tự!',
        'email.required' => 'Bạn chưa nhập email!',       
      ]);
        $user = new User;
        $user->name = $request->name;
        $user->email = $request->email;
        $user->address = $request->address;
        $user->password = Hash::make($request->password);
        $user->save();
        return redirect()->back()->with('thongbao','Bạn đã đăng ký thành công');
    }

    public function login(){
		// if(Auth::user())
		// return redirect()->route('admin.danhsach');
        // else
	   return view('admin.users.login');
   }

   public function postLogin(Request $request)
   {
	   $this->validate($request,
		   [
			   'email'=>'required|email',
			   'password' =>'required|min:6' 
		   ],
		   [
				'email.required' => 'Bạn chưa nhập Địa chỉ Email!',
				'email.email' => 'Email không đúng định dạng xxx@xxx.com!',
				'password.required' => 'Bạn chưa nhập Mật khẩu!',
				'password.min' => 'Mật Khẩu gồm tối thiểu 6 ký tự!'
			]);
			$remember = $request->has('remember') ? true : false;
	   if (Auth::attempt(['email'=>$request->email,'password'=>$request->password],$remember))
		{
		  return view('index');
		}
	   return redirect()->back()->with('thatbai','tài khoản hoặc mật khẩu sai');
   }

    public function logoutUser(){
        Auth::logout();
        return redirect()->route('admin.login');
    }
}
