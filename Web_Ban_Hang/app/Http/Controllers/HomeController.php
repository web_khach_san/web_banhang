<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use Session;
use App\Cart;

class HomeController extends Controller
{
   
    public function trangchu()
    { 
        $productType = Product::all();
        return view('template.body',compact('productType'));
    }

    public function admin()
    {
        return view('admin.layout.layout');
    }
}