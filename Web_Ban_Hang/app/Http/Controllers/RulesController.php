<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Rule;

class RulesController extends Controller
{

    public function DsQuytac()
    {
        $rules = Rule::all();
        return view('admin.rules.list', compact('rules'));
    }

    public function createRules()
    {
        return view('admin.rules.create');
    }

    public function storeRules(Request $request)
    {
        $rules = new Rule();

        $rules->content = $request->content;
        $rules->save();
        return redirect('admin/rules/DsQuytac')->with('message', 'Thêm thành công!');
    }


    public function deleteRules($id)
    {

        $rules = Rule::find($id);
        $rules->delete();
        return redirect()->back()->with('message', 'Xóa thành công!');
    }

    public function editRules($id)
    {
        $rules = Rule::findOrFail($id);
        return view('admin.rules.edit', compact('rules'));
    }

    public function updateRules(Request $request, $id)
    {
        $rules = Rule::findOrFail($id);

        $rules->content = $request->content;
        $rules->save();

        return redirect('admin/rules/DsQuytac')->with('message', 'Sửa thành công!');
    }
}