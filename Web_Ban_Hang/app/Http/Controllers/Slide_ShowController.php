<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\SlideShow;
use Illuminate\Support\Facades\Storage;

class Slide_ShowController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function DsSlide_show()
    {
        $slides = SlideShow::all();
        return view('admin.slide_shows.list', compact('slides'));
    }

    public function createSlide_show()
    {
        return view('admin.slide_shows.create');
    }

    public function storeSlide_show(Request $request)
    {
        $slides = new SlideShow();

        $slides->name = $request->name;
        if ($request->hasFile('imgSlide')) {
            $image = $request->file('imgSlide');
            $path = $image->store('slideImg', 'public');
            $slides->image = $path;
        }
        $slides->save();
        return redirect('admin/slide_show/DsSlide_show')->with('message', 'Thêm thành công!');
    }

    public function deleteSlide_show($id)
    {

        $slides = SlideShow::find($id);
        $image = $slides->image;
        if ($image) {
            Storage::delete('/public/' . $image);
        }
        $slides->delete();
        return redirect()->back()->with('message', 'Xóa thành công!');
    }

    public function editSlide_show($id)
    {
        $slides = SlideShow::findOrFail($id);
        return view('admin.slide_shows.edit', compact('slides'));
    }

    public function updateSlide_show(Request $request, $id)
    {
        $slides = SlideShow::findOrFail($id);

        $slides->name = $request->name;
        if ($request->hasFile('imgSlide')) {
            $image = $request->file('imgSlide');
            $path = $image->store('slideImg', 'public');
            $slides->image = $path;
        }
        $slides->save();

        return redirect('admin/slide_show/DsSlide_show')->with('message', 'Sửa thành công!');
    }
}