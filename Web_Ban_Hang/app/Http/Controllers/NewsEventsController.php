<?php

namespace App\Http\Controllers;

use App\NewsEvents;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class NewsEventsController extends Controller
{
    public function DsNew_Events()
    {
        $new_event = NewsEvents::all();
        return view('admin.news_events.list', compact('new_event'));
    }

    public function createNew_Events()
    {
        return view('admin.news_events.create');
    }

    public function storeNew_Events(Request $request)
    {
        $new_event = new NewsEvents();
        if ($request->hasFile('imgNewEvent')) {
            $image = $request->file('imgNewEvent');
            $path = $image->store('ImgNewEvent', 'public');
            $new_event->image = $path;
        }
        $new_event->title = $request->title;
        $new_event->content = $request->content;
        $new_event->save();
        return redirect('admin/news_events/DsNew_Events')->with('message', 'Thêm thành công!');
    }


    public function deleteNew_Events($id)
    {

        $new_event = NewsEvents::find($id);
        $image = $new_event->image;
        if ($image) {
            Storage::delete('/public/' . $image);
        }
        $new_event->delete();
        return redirect()->back()->with('message', 'Xóa thành công!');
    }

    public function editNew_Events($id)
    {
        $new_event = NewsEvents::findOrFail($id);
        return view('admin.news_events.edit', compact('new_event'));
    }

    public function updateNew_Events(Request $request, $id)
    {
        $new_event = NewsEvents::findOrFail($id);
        if ($request->hasFile('imgNewEvent')) {
            $image = $request->file('imgNewEvent');
            $path = $image->store('ImgNewEvent', 'public');
            $new_event->image = $path;
        }
        $new_event->title = $request->title;
        $new_event->content = $request->content;
        $new_event->save();

        return redirect('admin/news_events/DsNew_Events')->with('message', 'Sửa thành công!');
    }
}