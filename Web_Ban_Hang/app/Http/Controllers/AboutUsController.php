<?php

namespace App\Http\Controllers;

use App\AboutUs;
use Illuminate\Http\Request;

class AboutUsController extends Controller
{
    public function About_us()
    {
        $aboutus = AboutUs::all();
        return view('admin.about_us.list', compact('aboutus'));
    }
}