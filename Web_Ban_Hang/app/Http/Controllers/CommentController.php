<?php

namespace App\Http\Controllers;

use App\Comment;
use Illuminate\Http\Request;

class CommentController extends Controller
{
    
   

    
    public function createComment()
    {
        $comments = Comment::all();
        return view('admin.comment.create',compact('comments'));
    }

    public function postComment(Request $request)
    {
        $comments = new Comment;
        $comments->content = $request->content;
        $comments->save();
        return redirect()->back();
    }

   
    public function show(Comment $comment)
    {
        //
    }

    
    public function editComment(Request $request, $id)
    {
        
    }

   
    public function updateComment(Request $request,$id)
    {
        $comment = Comment::find($id);
        $comment->content = $request->content;
        $comment->save();
        return redirect()->back();
    }

    public function deleteComment($id)
    {
        //
    }
}
