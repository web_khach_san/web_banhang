<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;
use File;

class CategoryController extends Controller
{
    
    public function themLoaiSanPham()
    {
       return view('admin.category.add');
    }

    public function XuLyThemLoaiSanPham(Request $request)
    {
        $this->validate($request,
            [
                'name' => 'required',
                'description' => 'required|min:5',
            ],
            [
                'name.required' => 'Bạn chưa nhập tên sản phẩm!',
                'description.required' => 'Bạn chưa nhập mô tả về loại sản phẩm!',
                'description.min' => 'Mô tả quá ngắn, yêu cầu tối thiểu gồm 5 ký tự trở lên!',
            ]);
        $categories = new Category();

        $categories->name = $request->name;
        $categories->description = $request->description;
        if($request->hasFile('imageCategory')) 
    	{
    		$img_file = $request->file('imageCategory'); 
    		$img_file_name = $img_file->getClientOriginalName();
    		$img_file->move('upload/imageCategory',$img_file_name); 
    		$categories->image = $img_file_name;
    	}
    	else
    		$categories->image = ''; 
        $categories->save();

        return redirect('admin/san-pham/danh-sach-san-pham')->with('message', 'Thêm thành công!');
    }
    public function SuaLoaiSanPham($id)
    {
        $categories = Category::find($id);
        return view('admin.category.edit',compact('categories'));
    }

    public function XuLySuaLoaiSanPham(Request $request,$id)
    {
        $this->validate($request,
        [
            'name' => 'required',
            'description' => 'required|min:5',
        ],
        [
            'name.required' => 'Bạn chưa nhập tên sản phẩm!',
            'description.required' => 'Bạn chưa nhập mô tả về loại sản phẩm!',
            'description.min' => 'Mô tả quá ngắn, yêu cầu tối thiểu gồm 5 ký tự trở lên!',
        ]);
        $categories = Category::find($id);
        $categories->name = $request->name;
        $categories->description = $request->description;
        if($request->hasFile('imageCategory')) 
    	{
    		$img_file = $request->file('imageCategory'); 
            $img_file_name = $img_file->getClientOriginalName();
            if(File::exists('upload/imageCategory/'.$categories->image)){
                unlink('upload/imageCategory/'.$categories->image);
                $img_file->move('upload/imageCategory',$img_file_name); 
                $categories->image = $img_file_name;
            } else{
                $img_file->move('upload/imageCategory',$img_file_name); 
    		    $categories->image = $img_file_name;
            }
        }
        $categories->save();
        return redirect('admin/san-pham/danh-sach-san-pham')->with('message', 'Thêm thành công!');
        }
        public function xoaLoaiSanPham($id)
        {
            $categories =  Category::find($id);
            if(File::exists('upload/imageCategory/'.$categories->image)){
                unlink('upload/imageCategory/'.$categories->image);
            }
            $categories->delete();
            return redirect('admin/san-pham/danh-sach-san-pham')->with('message', 'Xóa thành công!');
        }
}
