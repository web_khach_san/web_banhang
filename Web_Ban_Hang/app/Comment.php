<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    public function Rep_comment(){
        return $this->hasMany(RepComments::class,'id_repComment','id');
    }
}
