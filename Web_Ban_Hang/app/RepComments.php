<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RepComments extends Model
{
    public function comment(){
        return $this->belongsTo(Comment::class,'id_repComment','id');
    }
}
